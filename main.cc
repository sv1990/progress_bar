#include "progress_bar.hh"

#include <chrono> // for namespace std::chrono_literals
#include <thread> // for std::this_thread::sleep_for

#include <cmath>

void f(int j) {
  auto x = std::sqrt(j);
  (void)x; // Prevent warning about unused variable
}

int main() {
  const int n = 1e5;
  progress_bar pb(n, 20); // Underestimates number of iterations
  for (int i = 0; i < 2 * n; ++i) {
    pb.make_progress();
    for (int j = 0; j < n; ++j) {
      f(j);
    }
  }

  pb.reset(2 * n, 20);
#pragma omp parallel for
  for (int i = -n; i < n; ++i) {
    ++pb;
    for (int j = 0; j < n; ++j) {
      f(j);
    }
  }
  pb.clear();

  using namespace std::chrono_literals;
  basic_progress_bar<'(', ' ', 'c', '.', ')'> gpb(6);
  // The sleeps represent some time consuming functions
  std::this_thread::sleep_for(1s);
  gpb.make_progress(1);
  std::this_thread::sleep_for(2s);
  gpb += 2;
  std::this_thread::sleep_for(1s);
  gpb.make_progress(1);
  std::this_thread::sleep_for(1s);
  gpb.make_progress(1);
  std::this_thread::sleep_for(1s);
  gpb.make_progress(1);
}
