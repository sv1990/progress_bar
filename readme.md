#progress bar

The classes `basic_progress_bar` and `progress_bar` provide simple progress bars for the terminal that can be used safely with openMP.
Three possible uses are shown in `main.cc`.

## Thread Safety Guarantees

It is only thread-safe to use a single `progress_bar` instance inside an openMP parallel scope. Multiple `progress_bar` instances can have a data race when print to `std::clog`.
