// http://bisqwit.iki.fi/story/howto/openmp/#Locks
#ifndef _OMP_MUTEX_HH_
#define _OMP_MUTEX_HH_

#ifdef _OPENMP
#  include <omp.h>
class omp_mutex {
  omp_lock_t _lock;

public:
  omp_mutex() noexcept { omp_init_lock(&_lock); }
  ~omp_mutex() { omp_destroy_lock(&_lock); }
  void lock() noexcept { omp_set_lock(&_lock); }
  void unlock() noexcept { omp_unset_lock(&_lock); }
  omp_mutex(const omp_mutex&) = delete;
  omp_mutex& operator=(const omp_mutex&) = delete;
};

#else
class omp_mutex {
public:
  constexpr void lock() const noexcept {}
  constexpr void unlock() const noexcept {}
};

#endif

// std::lock_guard should work as well
class omp_scoped_lock {
  omp_mutex& _mtx;

public:
  explicit omp_scoped_lock(omp_mutex& mtx) noexcept : _mtx(mtx) { _mtx.lock(); }
  ~omp_scoped_lock() noexcept { _mtx.unlock(); }
  omp_scoped_lock(const omp_scoped_lock&) = delete;
  omp_scoped_lock& operator=(const omp_scoped_lock&) = delete;
};

#endif // _OMP_MUTEX_HH_
