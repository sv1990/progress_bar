#ifndef _PROGRESS_BAR_HH_
#define _PROGRESS_BAR_HH_

// Define NOPROGRESS to disable the progress bar. For example when submitting to
// a cluster.
#ifndef NOPROGRESS

#  ifdef _OPENMP
#    include "omp_mutex.hh"
#  endif

#  include <algorithm>
#  include <atomic>
#  include <iomanip>
#  include <iostream>
#  include <iterator>
#  include <sstream>

template <char opening, char full, char current, char empty, char closing>
class basic_progress_bar {
  std::atomic<std::size_t> _progress;
  std::size_t _maxiterations;
  std::size_t _refreshes;
  std::size_t _width{};
  static const std::size_t _fixed_width{7}; // Width of braces + percentage
#  ifdef _OPENMP
  omp_mutex print_mutex;
#  endif

  void print_helper(std::ostream& os, std::size_t finished,
                    std::size_t unfinished) noexcept {
    os << opening;
    if (finished > 0) {
      std::fill_n(std::ostreambuf_iterator<char>{os}, finished - 1, full);
      os << current;
    }
    std::fill_n(std::ostreambuf_iterator<char>{os}, unfinished, empty);
    os << closing;
    const auto percentage = 100 * finished / (finished + unfinished);
    os << std::setw(4) << percentage << '%';
  }
  void print_progress(std::size_t finished, std::size_t unfinished) noexcept {
#  ifdef _OPENMP
    std::ostringstream oss;
    print_helper(oss, finished, unfinished);
    omp_scoped_lock lock(print_mutex);
    std::clog << oss.str();
#  else
    print_helper(std::clog, finished, unfinished);
#  endif
    std::clog << '\r';
    std::clog.flush();
  }

public:
  explicit basic_progress_bar(std::size_t maxiterations,
                              std::size_t refreshes = 20) noexcept {
    reset(maxiterations, refreshes);
  }
  void make_progress(std::size_t newprogress = 1) noexcept {
    _progress += newprogress;
    if (_progress % (_maxiterations / _refreshes) == 0) {
      const auto finished = _refreshes * _progress / _maxiterations;
      if (finished <= _refreshes) {
        print_progress(finished, _refreshes - finished);
      }
      else { // Number of iterations was underestimated. Progress bar needs to
             // grow
        _width = finished + _fixed_width;
        print_progress(finished, 0);
      }
    }
  }
  basic_progress_bar& operator+=(std::size_t new_progress) noexcept {
    make_progress(new_progress);
    return *this;
  }
  basic_progress_bar& operator++() noexcept {
    make_progress();
    return *this;
  }
  basic_progress_bar& operator++(int) noexcept { return ++(*this); }
  void reset(std::size_t maxiterations, std::size_t refreshes = 20) noexcept {
    clear();
    _progress      = 0;
    _maxiterations = maxiterations;
    _refreshes     = std::min(refreshes, maxiterations);
    _width         = _refreshes + _fixed_width;
    print_progress(0, _refreshes);
  }
  void clear() noexcept {
#  ifdef _OPENMP
    omp_scoped_lock lock(print_mutex);
#  endif
    std::fill_n(std::ostreambuf_iterator<char>{std::clog}, _width, ' ');
    std::clog << '\r';
    std::clog.flush();
  }
  ~basic_progress_bar() { clear(); }
};

#else // NOPROGRESS

#  include <string>

template <char opening, char full, char current, char empty, char closing>
class basic_progress_bar {
public:
  constexpr explicit basic_progress_bar(std::size_t, std::size_t = 20,
                                        const std::string& = "") noexcept {}
  constexpr basic_progress_bar(std::size_t, const std::string&) noexcept {}
  constexpr void make_progress(std::size_t = 1) const noexcept {}
  constexpr basic_progress_bar& operator+=(std::size_t) noexcept {
    return *this;
  }
  constexpr basic_progress_bar& operator++() noexcept { return *this; }
  constexpr basic_progress_bar& operator++(int) noexcept { return *this; }
  constexpr void reset(std::size_t, std::size_t = 20,
                       const std::string& = "") const noexcept {}
  constexpr void clear() const noexcept {}
};

#endif // NOPROGRESS

using progress_bar = basic_progress_bar<'[', '#', '#', '.', ']'>;

#endif // _PROGRESS_BAR_HH_
